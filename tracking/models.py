# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from datetime import date

# Create your models here.

class Parcel(models.Model):
    track_number = models.CharField(max_length=50)
    city = models.CharField(max_length=200)
    client_name = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=50)
    payment_summ = models.FloatField(max_length=200)
    departure_date = models.DateField(default=date.today)
    days_after_departure = models.PositiveSmallIntegerField()
    status = models.CharField(max_length=200)

#    def add_parcel(self):
#        self.save()
