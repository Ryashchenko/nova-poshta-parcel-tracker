from django import forms

from .models import Parcel

class ParcelForm(forms.ModelForm):

    class Meta:
        model = Parcel
        fields = ('track_number', 'city', 'client_name', 'phone_number', 'payment_summ', 'departure_date' )
