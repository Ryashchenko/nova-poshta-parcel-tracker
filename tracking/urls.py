from django.conf.urls import url
from . import views

urlpatterns = [
#    url(r'^delete_parcels/$', views.delete_parcels, name='delete_parcels'),
    url(r'^$', views.parcel_list, name='parcel_list'),
    url(r'^parcel/(?P<pk>\d+)/$', views.parcel_edit, name='parcel_edit'),
    url(r'^parcel/new/$', views.parcel_new, name='parcel_new'),
    url(r'^delete_parcels/$', views.delete_parcels, name='delete_parcels'),
]
