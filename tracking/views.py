# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect, get_object_or_404
from datetime import datetime, date
from .models import Parcel
from .forms import ParcelForm
import requests
import json
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages

# Create your views here.

def parcel_list(request):
    parcels = Parcel.objects.all()
    for iks in parcels:
        api_domain = 'https://api.novaposhta.ua'
        api_path = '/v2.0/json/'
        api_data = {
                'modelName': "TrackingDocument",
                'calledMethod': "getStatusDocuments",
                'methodProperties': {
                    "Documents": [
                        {
                        "DocumentNumber": iks.track_number
                        }
                    ]
                    }
                }
        r = requests.post(api_domain + api_path, json=api_data).json()
        parcel_status = r['data'][0]['Status']
        iks.status = parcel_status
        iks.save(update_fields=["status"])
        today = date.today()
        departure_date = iks.departure_date
        difference = abs(today - departure_date)
        difference = difference.days
        iks.days_after_departure = difference
        iks.save(update_fields=["days_after_departure"])
    return render(request, 'tracking/parcel_list.html', {'parcels': parcels} )

def parcel_new(request):
    if request.method == "POST":
        form = ParcelForm(request.POST)
        if form.is_valid():
            parcel = form.save(commit=False)
            parcel.days_after_departure = 0
            api_domain = 'https://api.novaposhta.ua'
            api_path = '/v2.0/json/'
            api_data = {
                    'modelName': "TrackingDocument",
                    'calledMethod': "getStatusDocuments",
                    'methodProperties': {
                        "Documents": [
                            {
                            "DocumentNumber": parcel.track_number
                            }
                        ]
                        }
                    }
            r = requests.post(api_domain + api_path, json=api_data).json()
            parcel_status = r['data'][0]['Status']
            parcel.status = parcel_status
            if parcel_status == "Номер не знайдено":
                messages.error(request,'Номер посылки не верный')
                return redirect('parcel_new')
            else:
                parcel.save()
                return redirect('parcel_list')
#            return render(request, 'tracking/parcel_list.html', {'parcel.status': parcel.status}, {'parcel.days_after_departure': parcel.days_after_departure})
    else:
        form = ParcelForm()
    return render(request, 'tracking/parcel_new.html', {'form': form})

def parcel_edit(request, pk):
    parcel = get_object_or_404(Parcel, track_number=pk)
    if request.method == "POST":
        form = ParcelForm(request.POST, instance=parcel)
        if form.is_valid():
            parcel = form.save(commit=False)
            parcel.days_after_departure = 0
            api_domain = 'https://api.novaposhta.ua'
            api_path = '/v2.0/json/'
            api_data = {
                    'modelName': "TrackingDocument",
                    'calledMethod': "getStatusDocuments",
                    'methodProperties': {
                        "Documents": [
                            {
                            "DocumentNumber": parcel.track_number
                            }
                        ]
                        }
                    }
            r = requests.post(api_domain + api_path, json=api_data).json()
            parcel_status = r['data'][0]['Status']
            parcel.status = parcel_status
            if parcel_status == "Номер не знайдено":
                messages.error(request,'Номер посылки не верный')
                return redirect('parcel_edit', pk=pk)
            else:
                parcel.save()
                return redirect('parcel_list')
    else:
        form = ParcelForm(instance=parcel)
    return render(request, 'tracking/parcel_edit.html', {'form': form})

@csrf_exempt
def delete_parcels(request, id=None):
    if request.method == 'POST':
        values = request.POST.getlist('checkbox_list')
        for parcel_id in values:
            Parcel.objects.get(id=parcel_id).delete()
        return redirect('parcel_list')
